var form = document.querySelector("form");
var userData = JSON.parse(localStorage.getItem("userData"));

form.addEventListener("submit", function (event) {
  event.preventDefault();
  var data = {
    email: form.email.value,
    password: form.password.value,
  };

  if (userData == null) {
    alert("Please create a new user account");
  } else if (
    userData.email == data.email &&
    userData.password == data.password
  ) {
    const isAuthenticated = true;
    localStorage.setItem("signin", JSON.stringify(userData));
    localStorage.setItem("isLoggedIn", JSON.stringify(isAuthenticated));
    window.location.href = "index.html";
  } else {
  }
});
