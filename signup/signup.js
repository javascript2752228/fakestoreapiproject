var form = document.querySelector("form");
form.addEventListener("submit", function (event) {
  event.preventDefault();
  console.log("hello");
  var data = {
    username: form.username.value,
    email: form.email.value,
    password: form.password.value,
  };

  window.location.href = "login.html";

  localStorage.setItem("userData", JSON.stringify(data));

  console.log(data);
});
