const isLoggedIn = JSON.parse(localStorage.getItem("isLoggedIn"));
console.log(isLoggedIn);
if (!isLoggedIn) {
  window.location.href = "signup.html";
}

const productsContainer = document.getElementById("products-container");
const loader = document.getElementById("loader");
loader.style.display = "block";
fetch("https://fakestoreapi.com/products")
  .then((response) => response.json())
  .then((products) => {
    loader.style.display = "none";
    if (products.length > 0) {
      fetchProductData(products);
    } else {
      productsContainer.textContent = "No products available.";
    }
  })
  .catch((error) => {
    loader.style.display = "none"; // Hide the loader
    productsContainer.textContent = "Error fetching products.";
    console.error("Error fetching products:", error);
  });

function fetchProductData(products) {
  // Fetching products from the API
  productsContainer.innerText = "";
  console.log(products);
  // Rendering products on the screen using a for loop.
  for (let item = 0; item < products.length; item++) {
    const product = products[item];

    const productCard = document.createElement("div");
    productCard.className = "product";
    productCard.id = product.id;
    productCard.addEventListener("click", () => {
      console.log("hello");
      console.log(product.id);
      productInformation(product.id, products);
    });

    const productImage = document.createElement("img");
    productImage.src = product.image;

    const productName = document.createElement("h2");
    productName.textContent = product.title;

    const productPrice = document.createElement("p");
    productPrice.className = "price";
    productPrice.textContent = `Price $${product.price}`;

    const productDescription = document.createElement("p");
    productDescription.textContent = product.description;
    // appending all the items to the product card
    productCard.appendChild(productImage);
    productCard.appendChild(productName);
    productCard.appendChild(productPrice);
    if (products.length === 1) {
      productCard.appendChild(productDescription);

      const button = document.createElement("button");
      button.textContent = "BACK";

      button.addEventListener("click", () => {
        location.reload();
      });
      productCard.appendChild(button);
      // Append the button to a container (e.g., body element)
      productCard.style.position = "fixed";

      productCard.style.top = "50%";
      productCard.style.left = "50%";
      productCard.style.transform = "translate(-50%, -50%)";
      productCard.style.background = "#fff";
      productCard.style.boxShadow = "0 4px 8px rgba(0, 0, 0, 0.1)";
      productCard.style.padding = "40px";
      productCard.style.borderRadius = "12px";
    }

    // appending productCard to the products container
    productsContainer.appendChild(productCard);
  }
}

function productInformation(id, products) {
  const productInfo = products.filter((item, index, array) => {
    return item.id === id;
  });
  fetchProductData(productInfo);
}
